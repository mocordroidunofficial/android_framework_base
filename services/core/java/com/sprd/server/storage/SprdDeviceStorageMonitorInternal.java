/* SPRD: created by spreadst */

package com.sprd.server.storage;

public interface SprdDeviceStorageMonitorInternal {
    public void beginGetShowStorageData();
    public long[] getShowStorageDataOK();
    public void setUpdateStorageDataFlag(boolean updateFlag);
}

