/*
 * Copyright (C) 2013 Sprdtrum.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import android.annotation.SdkConstant;
import android.annotation.SdkConstant.SdkConstantType;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.IBinder;
import android.os.INetworkManagementService;
import android.os.Message;
import android.os.Messenger;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Slog;
import java.nio.charset.StandardCharsets;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;

import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import com.android.internal.telephony.TelephonyIntents;

import android.bluetooth.BluetoothAdapter;

import android.hardware.fm.FmManager;
import android.hardware.fm.FmConsts.*;
import android.os.UserHandle;
import android.provider.Settings;

import java.util.List;
import java.util.HashMap;

/**
 * WcnManagerService monitor the CP2 status, it there is any exception
 * happens in CP2, it will tell Wifi/BT/FM to be disabled
 * @hide
 * special for bluetooth: kill bluetooth service after cp2 due to hw limitation
 */
//TODO: 

public class WcnManagerService {
    private static final String TAG = "WcnManagerService";
    private static final String WCND_TAG = "WcndConnector";

    private static final boolean DBG = true;
    private static boolean mResetEnable = true;

    //use to control if need to connect to wcnd
    private static boolean mWcnServiceEnable = true;


    private static final String WCND_CP2_EXCEPTION_STR = "WCN-CP2-EXCEPTION";
    private static final String WCND_CP2_RESET_START_STR =  "WCN-CP2-RESET-START";
    private static final String WCND_CP2_RESET_END_STR = "WCN-CP2-RESET-END";
    private static final String WCND_CP2_ALIVE_STR = "WCN-CP2-ALIVE";

    /**
     * Wait time in milliseconds before reset cp2
     */
    private static final int BEFORE_RESET_CP2_WAIT_TIME_MSECS = 1000;

    /**
     * Broadcast intent action indicating that CP2 status has been assert, alive,
     */
    @SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
    public static final String WCND_CP2_STATE_CHANGED_ACTION =  "com.android.server.wcn.WCND_CP2_STATE_CHANGED";

     /**
     * The lookup key for an {@code boolean} giving if CP2 is OK.
     */
    public static final String EXTRA_IS_CP2_OK = "isCP2OK";


     /**
     * The lookup key for an {@code String} giving the CP2 assert info.
     */
    public static final String EXTRA_CP2_ASSERT_INFO = "CP2AssertInfo";


    /**
     * used internally as a wifi connected event
     */
    private static final int EVENT_WIFI_CONNETED = 0;

    private static final int EVENT_WIFI_DISCONNETED = 1;

    private static final int EVENT_WIFI_SOFTAP_STARTED = 2;

    private static final int EVENT_WIFI_SOFTAP_STOPPED = 3;

    private static final int EVENT_LTE_BAND_CHANGED = 4;

    private static final int EVENT_PHONE_STATE_ON = 5;

    private static final int EVENT_PHONE_STATE_OFF = 6;


    private static final int EVENT_BT_STATE_CHANGED = 7;

    private static final int EVENT_WIFI_STATE_CHANGED = 8;

    private static final int EVENT_WIFI_SOFTAP_STATE_CHANGED = 9;


    private static final int WIFI_CHAN6_FREQ = 2437;

    /*
     * RSSI levels as used by notification icon
     * Level 4  -55 <= RSSI
     * Level 3  -66 <= RSSI < -55
     * Level 2  -77 <= RSSI < -67
     * Level 1  -88 <= RSSI < -78
     * Level 0         RSSI < -88
     */
    private static final int WIFI_LTE_RSSI_WATER_MARK = -70;


    /**
     * Binder context for this service
     */
    private Context mContext;
    
    private WifiManager mWifiManager = null;
    private WifiConfiguration mWifiApConfig = null;
    private boolean mWifiIsOpen = false;
    private boolean mWifiApIsOpen = false;
    private boolean mWifiScanOnlyIsOpen = false;

    private FmManager mFmManager = null;
    private boolean mFmIsOpen = false;


    private boolean mWifiConnected = false;


    private BluetoothAdapter mBluetoothAdapter = null;

    //to sync the WIFI/BT/FM status
    private final Object mSyncStatus = new Object();
    private boolean mWaitWifiClosed = false;
    private boolean mWaitBtClosed = false;
    private boolean mWaitFmClosed = false;
    private boolean mCP2Asserted = false;

    //polling count, max 10
    private int mWaitPollingCount = 0;

    //Command to check the WIFI BT FM status
    private static final int CMD_WIFIBTFM_STATE_POLLING = 26;



    private int mCurrentWifiFreq = 0;
    private int mCurrentWifiRssi = 0;


    private boolean mSoftApStarted = false;
    private int mCurrentSoftApChannel = 6;

    private volatile String mLTEBand;
    private volatile String mLTEInfo;

    private boolean mLteBandReportEnabled = false;
    private boolean mLte3wireSwitchEanbled = false;


    private INetworkManagementService mNwService;
    private TelephonyManager mTelephonyManager;
    private PhoneStateListener mPhoneListener;
    private boolean mPhoneOn = false;


    /**
     * connector object for communicating with wcnd
     */
    private NativeWcndConnector mConnector = null;

    private Thread mThread;

    private InternalHandler mHandler = null;


    private static final HashMap<String, Integer> softapBandMap = new HashMap<String, Integer>();

    //the band info from Tel/RIL
    static {
        softapBandMap.put("40", 11); //band 40
        softapBandMap.put("41", 1); //band 41
        softapBandMap.put("38", 1); //band 38
        softapBandMap.put("7", 1); // band 7
    }

    private static final HashMap<String, Integer> wifiBandMap = new HashMap<String, Integer>();

    //the band info from Tel/RIL
    static {
        wifiBandMap.put("40", 40); //band 40
        wifiBandMap.put("41", 41); //band 41
        wifiBandMap.put("38", 38); //band 38
        wifiBandMap.put("7", 7); //band 7
    }

    /**
     * Constructs a new WcnManagerService instance
     *
     * @param context  Binder context for this service
     */
    private WcnManagerService(Context context) {
        mContext = context;

        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        mFmManager = new FmManager(mContext);
        mConnector = new NativeWcndConnector();
        mThread = new Thread(mConnector, WCND_TAG);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //get the wcnreset property, default is enabled
        mResetEnable = SystemProperties.get("persist.sys.sprd.wcnreset","1").equals("1");
        log("mResetEnable = " + mResetEnable);

        //get the wcn modem property, default is disabled
        mWcnServiceEnable = SystemProperties.get("ro.modem.wcn.enable","0").equals("1");
        log("mWcnServiceEnable = " + mWcnServiceEnable);


        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mHandler = new InternalHandler(handlerThread.getLooper());


        IBinder b = ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE);
        mNwService = INetworkManagementService.Stub.asInterface(b);

        mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

        //Listen for phone state changed
        mPhoneListener = new PhoneStateListener() {
            @Override
            public void onServiceStateChanged(ServiceState serviceState) {
                log("Phone service state change: " + serviceState.getState());
                if (serviceState.getState() == ServiceState.STATE_POWER_OFF) {
                    //RADIO_NOT_AVAILABLE
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_PHONE_STATE_OFF));
                } else {
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_PHONE_STATE_ON));
                }
            }
        };
        //mTelephonyManager.listen(mPhoneListener, PhoneStateListener.LISTEN_SERVICE_STATE);


        //register Broadcast receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.NETWORK_IDS_CHANGED_ACTION);
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.CONFIGURED_NETWORKS_CHANGED_ACTION);
        filter.addAction(WifiManager.LINK_CONFIGURATION_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED); //check BT status

        //filter.addAction(TelephonyIntents.ACTION_BAND_INFO_REPORT);
        //if (!mTelephonyManager.isMultiSim()) {
        //    log("Single card, use default ACTION FILETER");

        //    filter.addAction(TelephonyIntents.ACTION_BAND_INFO_REPORT);
        //} else {

        //    log("Multi card!!!");

        //    for (int i=0; i<mTelephonyManager.getPhoneCount(); i++) {
        //        filter.addAction(TelephonyManager.getAction(TelephonyIntents.ACTION_BAND_INFO_REPORT, i));
        //    }
        //}

        mContext.registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        handleBroadcastEvent(context, intent);
                    }
                }, filter);


    }

    public static WcnManagerService create(Context context) throws InterruptedException {
        final WcnManagerService service = new WcnManagerService(context);

        Slog.d(TAG, "Creating WcnManagerService");

        if(mWcnServiceEnable) {
            service.mThread.start();
        }
        else {
            Slog.d(TAG, "Wcn is not enabled, do not need to start the service thread!!\n");
        }
        return service;
    }



    private void handleBroadcastEvent(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
            int wifi_state =intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                    WifiManager.WIFI_STATE_UNKNOWN);

            if ( wifi_state == WifiManager.WIFI_STATE_ENABLED ||wifi_state == WifiManager.WIFI_STATE_ENABLING) {
                log("Wifi is going to be opened!!");
            } else {
                log("Wifi is going to be closed!!");

                //if CP2 assert, to check the WIFI/BT/FM status, only care closed state
                if (mCP2Asserted) {
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_WIFI_STATE_CHANGED));
                }

            }

        } else if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
            NetworkInfo info = (NetworkInfo) intent.getParcelableExtra(
                    WifiManager.EXTRA_NETWORK_INFO);
            boolean isConnected = info.isConnected();
            //log("info: " + info);

            if (isConnected) {
                mHandler.sendMessage(mHandler.obtainMessage(EVENT_WIFI_CONNETED));
            } else {
                mHandler.sendMessage(mHandler.obtainMessage(EVENT_WIFI_DISCONNETED));
            }

        } else if (WifiManager.WIFI_AP_STATE_CHANGED_ACTION.equals(action)) {
            int softap_state = intent.getIntExtra(
                    WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);

            boolean softapStarted = false;
            if ( softap_state == WifiManager.WIFI_AP_STATE_ENABLING ||softap_state == WifiManager.WIFI_AP_STATE_ENABLED) {
                log("Wifi softap is going to be opened!!");

                softapStarted = true;
            } else {
                log("Wifi softap is going to be closed!!");

                softapStarted = false;

                //if CP2 assert, to check the WIFI/BT/FM status, only care about closed state
                if (mCP2Asserted) {
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_WIFI_SOFTAP_STATE_CHANGED));
                }

            }

            if (softapStarted) {
                mHandler.sendMessage(mHandler.obtainMessage(EVENT_WIFI_SOFTAP_STARTED));
            } else {
                mHandler.sendMessage(mHandler.obtainMessage(EVENT_WIFI_SOFTAP_STOPPED));
            }

         } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {//BT state changed
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.STATE_OFF);

                log("Recive BT State changed Notify: " + state);

                if (state != BluetoothAdapter.STATE_ON) {
                    //if CP2 assert, to check the WIFI/BT/FM status, only care about closed state
                    if (mCP2Asserted) {
                        mHandler.sendMessage(mHandler.obtainMessage(EVENT_BT_STATE_CHANGED));
                    }
                }
        }

    }



    private class InternalHandler extends Handler {
        public InternalHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            NetworkInfo info;
            switch (msg.what) {
                case EVENT_WIFI_CONNETED: {

                    break;

                }

                case EVENT_WIFI_DISCONNETED: {

                    break;
                }

                case EVENT_WIFI_SOFTAP_STARTED: {

                    break;
                }

                case EVENT_WIFI_SOFTAP_STOPPED: {

                    break;
                }

                case EVENT_LTE_BAND_CHANGED: {

                    break;
                }


                case EVENT_PHONE_STATE_ON: {

                    break;
                }


                case EVENT_PHONE_STATE_OFF: {

                    break;
                }

                case EVENT_BT_STATE_CHANGED:
                case EVENT_WIFI_STATE_CHANGED:
                case EVENT_WIFI_SOFTAP_STATE_CHANGED:
                case CMD_WIFIBTFM_STATE_POLLING: {

                    log("need to check Wifi/bt/fm state, mCP2Asserted:" + mCP2Asserted + ", event: " + msg.what);

                    if (!mCP2Asserted) break;

                    log("need to check Wifi/bt/fm state, mWaitWifiClosed:" + mWaitWifiClosed + ", mWaitBtClosed: " + mWaitBtClosed + ", mWaitFmClosed: " + mWaitFmClosed);

                    if (mWaitWifiClosed || mWaitBtClosed || mWaitFmClosed)
                        checkWifiBtFmStatusForCP2Exception();

                    break;
                }

            }
        }
    }


    private void log(String s) {
        Slog.d(TAG, s);
    }

    private void loge(String s) {
        Slog.e(TAG, s);
    }



//////////BELOW IS FOR CP2////////////////////////////////////////////////////////////


    private void sendWcnStateChangeBroadcast(final boolean isOK, String assertInfo) {
        Intent intent = new Intent(WCND_CP2_STATE_CHANGED_ACTION);
        intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        intent.putExtra(EXTRA_IS_CP2_OK, isOK);
        intent.putExtra(EXTRA_CP2_ASSERT_INFO, assertInfo);

        mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
    }

    private void disableWifiIfNeed() {
        if(mWifiManager == null) {
            loge("mWifiManager null !!!ERROR!!");
            return;
        }

        //check if WIFI_SCAN_ALWAYS_AVAILABLE is opened or not
        if((Settings.Global.getInt(mContext.getContentResolver(),Settings.Global.WIFI_SCAN_ALWAYS_AVAILABLE, 0)) == 1) {
            log("WIFI_SCAN_ALWAYS_AVAILABLE is opened, close it first!");
            mWifiScanOnlyIsOpen =true;
            Settings.Global.putInt(mContext.getContentResolver(),Settings.Global.WIFI_SCAN_ALWAYS_AVAILABLE, 0);
        }

        //disable wifi STA
        int wifiState = mWifiManager.getWifiState();
        if ((wifiState == WifiManager.WIFI_STATE_ENABLING) || (wifiState == WifiManager.WIFI_STATE_ENABLED)) {
            mWifiIsOpen = true;
            if (mWifiManager.setWifiEnabled(false)) {
                log("Shutdown WIFI because of WCND_CP2_EXCEPTION!!");
            } else {
                // Error
                loge("Shutdown WIFI because of WCND_CP2_EXCEPTION!!!ERROR!!");
            }
        }

        //disable wifi softap
        int wifiApState = mWifiManager.getWifiApState();
        if ((wifiApState == WifiManager.WIFI_AP_STATE_ENABLING) || (wifiApState == WifiManager.WIFI_AP_STATE_ENABLED)) {
            mWifiApIsOpen = true;
            mWifiApConfig = mWifiManager.getWifiApConfiguration();
            if (mWifiManager.setWifiApEnabled(null, false)) {
                log("Shutdown WIFI AP because of WCND_CP2_EXCEPTION!!");
            } else {
                // Error
                loge("Shutdown WIFI AP because of WCND_CP2_EXCEPTION!!!ERROR!!");
            }
        }
    }

    private void disableFM() {
        if(mFmManager == null) {
            loge("mFmManager null !!!ERROR!!");
            return;
        }

        if (mFmManager.isFmOn()) {
            mFmIsOpen = true;
            if (mFmManager.powerDown()) {
                log("Shutdown FM because of WCND_CP2_EXCEPTION!!");
            } else {
                // Error
                loge("Shutdown FM because of WCND_CP2_EXCEPTION!!!ERROR!!");
            }
        }
    }

    private void enableWifiIfNeed() {
        if(mWifiManager == null) {
            loge("mWifiManager null !!!ERROR!!");
            return;
        }

        //enable wifi STA, if it is disabled before.
        if (mWifiIsOpen) {
            mWifiIsOpen = false;
            if (mWifiManager.setWifiEnabled(true)) {
                log("Open WIFI because of WCND_CP2_ALIVE again!!");
            } else {
                // Error
                loge("Open WIFI because of WCND_CP2_ALIVE again!!!ERROR!!");
            }
        }

        //enable WIFI_SCAN_ALWAYS_AVAILABLE again, if it is closed before
        if(mWifiScanOnlyIsOpen){
            log("resume WIFI_SCAN_ALWAYS_AVAILABLE");
            mWifiScanOnlyIsOpen =false;
            Settings.Global.putInt(mContext.getContentResolver(),Settings.Global.WIFI_SCAN_ALWAYS_AVAILABLE, 1);
        }

        //enable wifi softap,  if it is disabled before.
        if (mWifiApIsOpen) {
            mWifiApIsOpen = false;
            if (mWifiManager.setWifiApEnabled(mWifiApConfig, true)) {
                log("Open WIFI AP because of WCND_CP2_ALIVE again!!");
            } else {
                // Error
                loge("Open WIFI AP because of WCND_CP2_ALIVE again!!!ERROR!!");
            }
        }
    }

    private void enableFM() {
        if(mFmManager == null) {
            loge("mFmManager null !!!ERROR!!");
            return;
        }

        //enable wifi Bluetooth, if it is disabled before.
        if (mFmIsOpen) {
            mFmIsOpen = false;
            if (mFmManager.powerUp()) {
                //mFmManager.setAudioPath(FmAudioPath.FM_AUDIO_PATH_HEADSET);
                log("Open FM because of WCND_CP2_ALIVE again!!");
            } else {
                // Error
                loge("Open FM because of WCND_CP2_ALIVE again!!!ERROR!!");
            }
        }
    }


    //check if wifi opend, include WIFI_SCAN_ALWAYS_AVAILABLE /WIFI STA / WIFI SOFTAP
    private boolean isWifiOpened() {
        if(mWifiManager == null) {
            loge("mWifiManager null !!!ERROR!!");
            return false;
        }

        //check if WIFI_SCAN_ALWAYS_AVAILABLE is opened or not
        if((Settings.Global.getInt(mContext.getContentResolver(),Settings.Global.WIFI_SCAN_ALWAYS_AVAILABLE, 0)) == 1) {
            return true;
        }

        //disable wifi STA
        int wifiState = mWifiManager.getWifiState();
        if (wifiState == WifiManager.WIFI_STATE_ENABLED) {
            return true;
        }

        //disable wifi softap
        int wifiApState = mWifiManager.getWifiApState();
        if (wifiApState == WifiManager.WIFI_AP_STATE_ENABLED) {
            return true;
        }

        return false;
    }

    //to check the WIFI/BT/FM status
    private void checkWifiBtFmStatusForCP2Exception() {

        //for Wifi
        if (isWifiOpened()) {

            log("Wifi opened!! (mWifiScanOnlyIsOpen:" + mWifiScanOnlyIsOpen + ",  mWifiIsOpen:" + mWifiIsOpen + ", mWifiApIsOpen:" + mWifiApIsOpen);

            mWaitWifiClosed = true;
        } else {
            mWaitWifiClosed = false;
        }

        //for BT
        if (mBluetoothAdapter != null ) {
            int bluetoothState = mBluetoothAdapter.getState();
            if ((bluetoothState == BluetoothAdapter.STATE_ON)) {
                log("BT opened!!");
                mWaitBtClosed = true;
            }
        } else {
            mWaitBtClosed = false;
        }


        //for FM
        if ((mFmManager != null) && mFmManager.isFmOn()) {
                log("FM opened!!");
                mWaitFmClosed = true;
        } else {
            mWaitFmClosed = false;
        }


        mWaitPollingCount++;

        //
        if ((mWaitPollingCount < 10) &&  (mWaitWifiClosed || mWaitBtClosed || mWaitFmClosed)) {

            mHandler.removeMessages(CMD_WIFIBTFM_STATE_POLLING);//

            mHandler.sendMessageDelayed(mHandler.obtainMessage(CMD_WIFIBTFM_STATE_POLLING), 1000);//

        } else {
            log("WIFI/BT/FM all closed!!!");
            mHandler.removeMessages(CMD_WIFIBTFM_STATE_POLLING);//

            mWaitWifiClosed = false;
            mWaitBtClosed = false;
            mWaitFmClosed = false;

            resetCP2();
        }

    }

    private void receiveCP2Exception(String assertInfo) {
        mWaitPollingCount = 0;

        log( "receiveCP2Exception function");
        //send broadcast anyway
        sendWcnStateChangeBroadcast(false, assertInfo);

        //if reset enabled, then do the related operations
        //if(mResetEnable) {
        //    disableWifi();
        //}

        //For marlin before dump mem or reset , need to wait WIFI/BT/FM to be stopped
        disableWifiIfNeed();
        checkWifiBtFmStatusForCP2Exception();

        mCP2Asserted = true;
    }

    private void receiveCP2Alive(String aliveInfo) {
        mCP2Asserted = false;
        mWaitPollingCount = 0;

        //send broadcast anyway
        sendWcnStateChangeBroadcast(true, aliveInfo);

        //if reset enabled, then do the related operations
        if(mResetEnable) {
            enableWifiIfNeed();
        }
    }

    private void resetCP2(){
        String resetcmd = "wcn reset";
        try{
              mConnector.sendCmd(resetcmd);
        }catch (Exception e) {
              loge("Error handling '" + resetcmd + "': " + e);
        }
    }

    private boolean handleMessage(String msg) {
        try {
            if (msg.contains(WCND_CP2_EXCEPTION_STR) ) {
                log( "Got WCND_CP2_EXCEPTION event [" + msg + "]");

                 //TODO
                receiveCP2Exception(msg);

                if (mResetEnable) {

                    //Need Wait for a while for WIFI/BT/FM to be disabled???
                    SystemClock.sleep(BEFORE_RESET_CP2_WAIT_TIME_MSECS);

                    //tell wcnd to reset cp2
                    //resetCP2();

                } else {
                    //tell wcnd to reset cp2, but wcnd will check if reset is enabled or not.
                    //resetCP2();
                }
            }

            if (msg.contains(WCND_CP2_RESET_START_STR) ) {
                log( "Got WCND_CP2_RESET_START event [" + msg + "]");
                //TODO
            }

            if (msg.contains(WCND_CP2_RESET_END_STR) ) {
                log( "Got WCND_CP2_RESET_END event [" + msg + "]");
                //TODO
            }

            if (msg.contains(WCND_CP2_ALIVE_STR) ) {
                log( "Got WCND_CP2_ALIVE_STR event [" + msg + "]");
                //TODO

                receiveCP2Alive(msg);

            }
        } catch (Exception e) {
            loge("Error handling '" + msg + "': " + e);
        }
        return true;
    }

    class NativeWcndConnector implements Runnable{
        private static final String mSocketName = "wcnd";


        private OutputStream mOutputStream;

        private final int BUFFER_SIZE = 4096;

        NativeWcndConnector() {
        }
    
        @Override
        public void run() {
           while (true) {
                try {
                    listenToSocket();
                } catch (Exception e) {
                    loge("Error in NativeWcndConnector: " + e);
                    SystemClock.sleep(5000);
                }
            }
        }

        public void sendCmd(String strcmd){
            if(mOutputStream != null){
                final StringBuilder cmdBuilder =  new StringBuilder(strcmd).append('\0');
                final String cmd = cmdBuilder.toString(); /* Cmd + \0 */
                try {
                    mOutputStream.write(cmd.getBytes(StandardCharsets.UTF_8));
                } catch (IOException e) {
                    loge("Failed wrirting output stream: " + e);
                }
            }
        }

        private void listenToSocket() throws IOException {
            LocalSocket socket = null;

            try {
                socket = new LocalSocket();
                LocalSocketAddress address = new LocalSocketAddress(mSocketName,
                        LocalSocketAddress.Namespace.ABSTRACT);

                socket.connect(address);

                InputStream inputStream = socket.getInputStream();

                mOutputStream = socket.getOutputStream();


                byte[] buffer = new byte[BUFFER_SIZE];
                int start = 0;

                while (true) {
                    int count = inputStream.read(buffer, start, BUFFER_SIZE - start);
                    if (count < 0) {
                        loge("got " + count + " reading with start = " + start);
                        break;
                    }

                    // Add our starting point to the count and reset the start.
                    count += start;
                    start = 0;

                    //Note: message send from wcnd must end with null
                    for (int i = 0; i < count; i++) {
                        if (buffer[i] == 0) {
                            final String rawEvent = new String(
                                    buffer, start, i - start, StandardCharsets.UTF_8);
                            log("WCND RCV <- {" + rawEvent + "}");

                            try {
                                handleMessage(rawEvent);
                            } catch (Exception e) {
                                log("Problem parsing message: " + rawEvent + " - " + e);
                            }

                            start = i + 1;
                        }
                    }
                    if (start == 0) {
                        final String rawEvent = new String(buffer, start, count, StandardCharsets.UTF_8);
                        log("WCND RCV incomplete <- {" + rawEvent + "}");
                    }

                    // We should end at the amount we read. If not, compact then
                    // buffer and read again.
                    if (start != count) {
                        final int remaining = BUFFER_SIZE - start;
                        System.arraycopy(buffer, start, buffer, 0, remaining);
                        start = remaining;
                    } else {
                        start = 0;
                    }
                }
            } catch (IOException ex) {
                loge("Communications error: " + ex);
                throw ex;
            } finally {

                    if (mOutputStream != null) {
                        try {
                            loge("closing stream for " + mSocketName);
                            mOutputStream.close();
                        } catch (IOException e) {
                            loge("Failed closing output stream: " + e);
                        }
                        mOutputStream = null;
                    }

                try {
                    if (socket != null) {
                        socket.close();
                    }
                } catch (IOException ex) {
                    loge("Failed closing socket: " + ex);
                }
            }
        }

    }

}
