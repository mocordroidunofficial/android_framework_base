/** Create by Spreadst */
package com.android.server;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;


public class ShutDownWakeLock {
	private static PowerManager.WakeLock sCpuWakeLock;

    static void acquireCpuWakeLock(Context context) {
        Log.v("ShutDownWakelock","Acquiring cpu wake lock");
        if (sCpuWakeLock != null) {
            return;
        }

        PowerManager pm =
                (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        sCpuWakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "ShutDown");
        sCpuWakeLock.acquire();
    }

    static void releaseCpuLock() {
        Log.v("ShutDownWakelock","Releasing cpu wake lock");
        if (sCpuWakeLock != null) {
            sCpuWakeLock.release();
            sCpuWakeLock = null;
        }
    }
}