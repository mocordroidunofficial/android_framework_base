// Only workaround for CTS Test
// TODO This file may move into independency service future
package com.android.server.am;

import android.os.SystemProperties;
import android.util.Log;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashMap;

public class CompatibilityTestUtil {

    static interface WorkaroundsCase {
        public void onStart();
        public void onStop();
    }

    private static final HashMap<String,WorkaroundsCase> CASE_MAP;

    static {
        CASE_MAP = new HashMap<String,WorkaroundsCase>();
        CASE_MAP.put("android.app.cts.uiautomation", new DvfsWorkarounds());
        CASE_MAP.put("android.theme.app", new DvfsWorkarounds());
        CASE_MAP.put("android.core.tests.libcore.package.harmony_java_net",
                new PropertiesWorkarounds("sys.data.ipv6.on", "1", "0"));
    }

    static void assumeAndPerformStart(String packageName) {
        if (packageName != null && CASE_MAP.containsKey(packageName)) {
            Log.d("CTS", "case " + packageName + "workaround start");
            CASE_MAP.get(packageName).onStart();
        }
    }

    static void assumeAndPerformEnd(String packageName) {
        if (packageName != null && CASE_MAP.containsKey(packageName)) {
            Log.d("CTS", "case " + packageName + "workaround end");
            CASE_MAP.get(packageName).onStop();
        }
    }

    static class DvfsWorkarounds implements WorkaroundsCase {
//  echo 1 > /sys/devices/system/cpu/cpufreq/sprdemand/cpu_hotplug_disable
//  echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
        private static final String DVFS_HOTPLUG_NODE = "/sys/devices/system/cpu/cpufreq/sprdemand/cpu_hotplug_disable";
        private static final String DVFS_GOVERNOR = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor";

        private boolean mDvfsClosed = false;

        static final boolean write(String node, String message) {
            boolean success = false;
            FileWriter writter = null;
            try {
                writter = new FileWriter(node);
            } catch (IOException ioe) {
                Log.e("CTS", "Failed to open " + node, ioe);
            }
            if (writter != null) {
                try {
                writter.write(message);
                } catch (IOException ioe) {
                    Log.e("CTS", "Failed to write " + message + " to " + node, ioe);
                }
                try {
                    writter.close();
                    success = true;
                } catch (IOException ioe) {
                    Log.e("CTS", "Failed to close writter " + node, ioe);
                }
            }
            return success;
        }

        @Override
        public void onStart() {
            // DVFS close once
            if (mDvfsClosed) return;
            write(DVFS_HOTPLUG_NODE, "1");
            mDvfsClosed = write(DVFS_GOVERNOR, "performance");
        }

        @Override
        public void onStop() {
            // No need to restart, it will be reset on next boot.
        }
    }

    static class PropertiesWorkarounds implements WorkaroundsCase {
        private final String mProperty;
        private final String mCurrent;
        private final String mOrigin;

        protected PropertiesWorkarounds(String property, String current, String origin) {
            mProperty = property;
            mCurrent = current;
            mOrigin = origin;
        }

        @Override
        public void onStart() {
            if (mCurrent != null) {
                SystemProperties.set(mProperty, mCurrent);
                Log.d("CTS", String.format("Setting %s to %s", mProperty, mCurrent));
            }
        }

        @Override
        public void onStop() {
            if (mOrigin != null) {
                SystemProperties.set(mProperty, mOrigin);
                Log.d("CTS", String.format("Setting %s to %s", mProperty, mCurrent));
            }
        }
    }
}
