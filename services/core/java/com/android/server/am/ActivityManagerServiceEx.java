/*opyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.am;

import android.app.ActivityManager;

import java.util.HashMap;
import java.util.HashSet;

//import android.app.IAlarmManager;
import android.app.IApplicationThread;
import android.os.ServiceManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.android.server.am.ActivityManagerService;

import android.os.Process;
import android.util.Log;
import android.util.Slog;
import android.util.SparseIntArray;
import android.os.SystemClock;

/**

 */
public final class ActivityManagerServiceEx extends ActivityManagerService {

    static final String TAG = "ActivityManagerEx";
    boolean DEBUG_AMSEX = false && !IS_USER_BUILD;
    // 15M, if the chached CHACH_PSS_MAX_VALUE , it adj will adjust to 8
    static final int CHACH_PSS_MAX_VALUE = 15 * 1025;
    boolean mIsInHome;
    
    public ActivityManagerServiceEx(Context systemContext) {
        super(systemContext);
        mIsInHome = true;
        // TODO Auto-generated constructor stub
    }

    boolean isTopHostApp(ProcessRecord app,  ProcessRecord TOP_APP){
        if( TOP_APP != null 
              && TOP_APP != mHomeProcess            
           && app != mHomeProcess){
            for (int i = 0; i < TOP_APP.activities.size(); i++){
               final ActivityRecord r = TOP_APP.activities.get(i);
               if(r != null  && r.callerPid== app.pid){
                           return true;
               }
            }                
        }
        return false;
   }

    /* AMS Optimization:3 */
    protected int computeOomAdjLocked(ProcessRecord app, int cachedAdj, ProcessRecord TOP_APP,
            boolean doingAll, long now) {
        mIsInHome = TOP_APP == mHomeProcess;
        int curadj = super.computeOomAdjLocked(app, cachedAdj, TOP_APP, doingAll, now);
        if (DEBUG_AMSEX)
            Slog.d(TAG, "computeOomAdjLocked enter app:" + app + " curadj:" + curadj);
        /* check State protection */
        switch (app.protectStatus) {
            case ActivityManager.PROCESS_STATUS_RUNNING:
            case ActivityManager.PROCESS_STATUS_MAINTAIN:
            case ActivityManager.PROCESS_STATUS_PERSISTENT: {
                int value = procProtectConfig.get(app.protectStatus);
                if(curadj > value) curadj = value;
                break;
            }
            case ActivityManager.PROCESS_PROTECT_CRITICAL:
            case ActivityManager.PROCESS_PROTECT_IMPORTANCE:
            case ActivityManager.PROCESS_PROTECT_NORMAL: {
                if (curadj >= app.protectMinAdj && curadj <= app.protectMaxAdj) {
                    int value = procProtectConfig.get(app.protectStatus);
                    if(curadj > value) curadj = value;
                }
                break;
            }
        }
        if(curadj > ProcessList.SERVICE_ADJ){
            if(app !=  TOP_APP && isTopHostApp(app, TOP_APP)){
                curadj = ProcessList.SERVICE_ADJ;
                app.cached = false;
                app.adjType = "host";
            }
        }
        if (DEBUG_AMSEX)
            Slog.d(TAG, "computeOomAdjLocked app.protectStatus:" + app.protectStatus + " app.protectMinAdj:" + app.protectMinAdj
                    + " protectMaxAdj:" + app.protectMaxAdj + " curadj:" + curadj);
        if (curadj != app.curRawAdj) {
            // adj changed, adjust its other parameters:
            app.empty = false;
            app.cached = false;
            //app.keeping = true; 5.0--already removed
            app.adjType = "protected";
            app.curProcState = ActivityManager.PROCESS_STATE_IMPORTANT_BACKGROUND;
            app.curSchedGroup = Process.THREAD_GROUP_DEFAULT;
            app.curAdj = curadj;
            if (DEBUG_AMSEX)
                Slog.d(TAG, "computeOomAdjLocked :" + app + " app.curAdj:" + app.curAdj);
        }
        return curadj;
    }

    /**
     * SPRD: getTotalCpuUsage
     * @hide
     */
    final public float getTotalCpuUsage() {
        synchronized (mProcessCpuTracker) {
            final long now = SystemClock.uptimeMillis();
            if (MONITOR_CPU_USAGE &&
                    mLastCpuTime.get() < (now - MONITOR_CPU_MIN_TIME)) {
                mLastCpuTime.set(now);
                mProcessCpuTracker.update();
            }
        }
        if (DEBUG_AMSEX)
            Slog.d(TAG, "getTotalCpuUsage :" + mProcessCpuTracker.getTotalCpuPercent());
        return mProcessCpuTracker.getTotalCpuPercent();
    }

    /**
     * SPRD: setProcessProtectStatus
     * @hide
     */
    public void setProcessProtectStatus(int pid, int status) {
        ProcessRecord app = null;
        synchronized (mPidsSelfLocked) {
            app = mPidsSelfLocked.get(pid);
        }
        if (app != null) {
            if (DEBUG_AMSEX)
                Slog.d(TAG, "setProcessProtectStatus, app: " + app + " status: " + status + " preStatus: " + app.protectStatus);
            synchronized (this) {
                app.protectStatus = status;
            }
        }
    }

    /**
     * SPRD: setProcessProtectStatus
     * @hide
     */
    public void setProcessProtectStatus(String appName, int status) {
        if (appName == null)
            return;
        if (DEBUG_AMSEX)
            Slog.d(TAG, "setProcessProtectStatus :" + appName + " status:" + status);
        for (int i = mLruProcesses.size() - 1; i >= 0; i--) {
            ProcessRecord rec = mLruProcesses.get(i);
            if (rec != null && rec.processName.equals(appName)) {
                rec.protectStatus = status;
                if (DEBUG_AMSEX)
                    Slog.d(TAG, "setProcessProtectStatus find app:" + rec);
                break;
            }
        }

    }

    /**
     * SPRD: setProcessProtectArea
     * @hide
     */
    public void setProcessProtectArea(String appName, int minAdj, int maxAdj, int protectLevel) {
        if (DEBUG_AMSEX)
            Slog.d(TAG, "setProcessProtectStatus :" + appName + " minAdj:" + minAdj + " maxAdj:"
                    + maxAdj + " protectLevel:" + protectLevel);
        if (appName == null)
            return;
        synchronized (mPidsSelfLocked) {
            for (int i = mLruProcesses.size() - 1; i >= 0; i--) {
                ProcessRecord rec = mPidsSelfLocked.valueAt(i);
                if (rec != null && rec.processName.equals(appName)) {
                    rec.protectStatus = protectLevel;
                    rec.protectMinAdj = minAdj;
                    rec.protectMaxAdj = maxAdj;
                    if (DEBUG_AMSEX)
                        Slog.d(TAG, "setProcessProtectArea find app:" + rec);
                    break;
                }
            }
        }
    }

    public static class ProtectArea
    {
        int mMinAdj;
        int mMaxAdj;
        int mLevel;

        public ProtectArea(int minAdj, int maxAdj, int protectLevel)
        {
            mMinAdj = minAdj;
            mMaxAdj = maxAdj;
            mLevel = protectLevel;
        }

        @Override
        public String toString() {
            return "ProtectArea [mMinAdj=" + mMinAdj + ", mMaxAdj=" + mMaxAdj + ", mLevel="
                    + mLevel + "]";
        }
        
    }

    static HashMap<String, ProtectArea> preProtectAreaList;
    static HashSet<String> hasAlarmList;
    static HashSet<String> needCleanPackages;
    static SparseIntArray procProtectConfig;
    
    static {
        preProtectAreaList = new HashMap<String, ProtectArea>();
        
        hasAlarmList = new HashSet<String>();
        hasAlarmList.add("com.baidu.searchbox");
        hasAlarmList.add("com.baidu.searchbox:pushservice_v1");
        hasAlarmList.add("com.facebook.katana");
        hasAlarmList.add("com.sina.weibo");
        hasAlarmList.add("com.tencent.mm:push");

        needCleanPackages = new HashSet<String>();
        needCleanPackages.add("com.google.android.gms");
        
        procProtectConfig = new SparseIntArray();
        procProtectConfig.put(ActivityManager.PROCESS_STATUS_RUNNING, 0);
        procProtectConfig.put(ActivityManager.PROCESS_STATUS_MAINTAIN, 2);
        procProtectConfig.put(ActivityManager.PROCESS_STATUS_PERSISTENT, -12);
        procProtectConfig.put(ActivityManager.PROCESS_PROTECT_CRITICAL, 0);
        procProtectConfig.put(ActivityManager.PROCESS_PROTECT_IMPORTANCE, 2);
        procProtectConfig.put(ActivityManager.PROCESS_PROTECT_NORMAL, 4);
    }

    protected void startProcessLocked(ProcessRecord app,
            String hostingType, String hostingNameStr) {
        super.startProcessLocked(app, hostingType, hostingNameStr);
        if (app.processName != null) {
            ProtectArea pa = preProtectAreaList.get(app.processName);
            if (pa != null) {
                app.protectStatus = pa.mLevel;
                app.protectMinAdj = pa.mMinAdj;
                app.protectMaxAdj = pa.mMaxAdj;
            }
            if (DEBUG_AMSEX)
                Slog.d(TAG, "startProcessLocked app.protectLevel :" + app.protectStatus
                        + " app.protectMinAdj :" + app.protectMinAdj
                        + " app.protectMaxAdj" + app.protectMaxAdj);
        }
    }
//	boolean cleanUpApplicationRecordLocked(ProcessRecord app,
//		   boolean restarting, boolean allowRestart, int index) {
//		   if(app != null && app.info != null && needCleanPackages.contains(app.info.packageName)){
//			   mFgBroadcastQueue.cleanDiedAppBroadcastLocked(app);
//			   mBgBroadcastQueue.cleanDiedAppBroadcastLocked(app);                    
//		   }
//             super.cleanUpApplicationRecordLocked(app, restarting, allowRestart,index); 
//		}

//    void appDiedLocked(ProcessRecord app, int pid,
//            IApplicationThread thread) {
//        if (hasAlarmList.contains(app.processName)
//		 ||(app.info !=null && needCleanPackages.contains(app.info.packageName))) {
//            ApplicationInfo diedAppInfo = app.instrumentationInfo != null
//                    ? app.instrumentationInfo : app.info;
//            final String roguePack = new String(diedAppInfo.packageName);
//            mHandler.post(new Runnable() {
//                public void run() {
//                    try {
//                        IAlarmManager alarmService = IAlarmManager.Stub.asInterface(ServiceManager
//                                .getService("alarm"));
//                        if (alarmService != null
//                                && alarmService.checkAlarmForPackageName(roguePack)) {
//                            alarmService.removeAlarmForPackageName(roguePack);
//                            if (DEBUG_AMSEX)
//                                Slog.w(TAG, "RemoveSvcAlarm: pkg=" + roguePack);
//                        }
//                    } catch (Exception e) {
//                        if (DEBUG_AMSEX)
//                            Slog.e(TAG, "RemoveSvcAlarm: Error!! " + e);
//                    }
//                }
//            });
//        }
//        super.appDiedLocked(app, pid, thread);
//    }

    public void addProtectArea(final String processName, final ProtectArea area) {
        if(processName == null || area == null) {
            return;
        }
        Slog.d(TAG, "addProtectArea, processName: " + processName + " ProtectArea: " + area);
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                synchronized (ActivityManagerServiceEx.this) {
                    preProtectAreaList.put(processName, area);
                    updateOomAdjLocked();
                }
            }
            
        });
    }
    
    public void removeProtectArea(final String processName) {
        if(processName == null) {
            return;
        }
        preProtectAreaList.remove(processName);
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                synchronized (ActivityManagerServiceEx.this) {
                    ProcessRecord app = null;
                    for(int i = mLruProcesses.size() -1; i >= 0; i--) {
                        if(processName.equals(mLruProcesses.get(i).processName)) {
                            app = mLruProcesses.get(i);
                            break;
                        }
                    }
                    Slog.d(TAG, "removeProtectArea, processName: " + processName + " app: " + app);
                    if(app != null) {
                        updateOomAdjLocked(app);
                    }
                }
            }
            
        });
    }
}


