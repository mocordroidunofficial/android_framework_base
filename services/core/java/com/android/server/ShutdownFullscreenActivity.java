/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** Create by Spreadst */
package com.android.server;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Slog;
import android.view.Window;
import android.view.WindowManager;
import android.view.View;

import com.android.server.power.ShutdownThread;

import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;
import android.media.MediaPlayer;
import android.content.ContentResolver;
import android.provider.Settings;
import java.io.IOException;
import android.net.Uri;
import android.text.TextUtils;
/* SPRD: ringtone,changed for bug303604 @{ */
import android.media.AudioManager;
/* @} */

public class ShutdownFullscreenActivity extends Activity {

    private static final String TAG = "ShutdownFullScreenActivity";
    private boolean mConfirm;
    //SPRD: add shutdown reason for PhoneInfo feature
    private String mShutDownMode = "timer";
    private int mSeconds = 15;
    private AlertDialog mDialog;
    /* SPRD: ringtone,changed for bug303604 @{ */
    private AudioManager mAudioManager;
    /* @} */
    MediaPlayer mplayer = new MediaPlayer();
    private Handler myHandler = new Handler();
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            mSeconds --;
            // Modify Bug 332142, Distinguish between singular and plura
            if (mSeconds != 0) {
                mDialog.setMessage(getResources().getQuantityString(
                        com.android.internal.R.plurals.shutdown_after_seconds_plurals, mSeconds,
                        mSeconds));
            } else {
                mDialog.setMessage(getString(com.android.internal.R.string.shutdown_confirm));
            }
            // bug 332142 end
            if(mSeconds > 0){
                myHandler.postDelayed(myRunnable,1000);
            }else{
                myHandler.post(new Runnable() {
                    public void run() {
                        mplayer.stop();
                        /* SPRD: add shutdown reason for PhoneInfo feature @{ */
                        Slog.i(TAG,"mShutDownMode = "+mShutDownMode);
                        ShutdownThread.shutdown(ShutdownFullscreenActivity.this, mConfirm, mShutDownMode);
			/* @} */
                    }
                });
            }
        }
    };

    private BroadcastReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // SPRD: Modify MediaPlayer play process for bug 257397
        final Context context = this;
        mConfirm = getIntent().getBooleanExtra(Intent.EXTRA_KEY_CONFIRM, false);
       //SPRD: add shutdown reason for PhoneInfo feature
	mShutDownMode = getIntent().getStringExtra("shutdown_mode");
        Slog.i(TAG, "onCreate(): confirm=" + mConfirm);

        /* SPRD: ringtone,changed for bug303604 @{ */
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        /* @} */

        if(getIntent().getBooleanExtra("can_be_cancel", false)) {
                mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(Intent.ACTION_BATTERY_OKAY.equals(intent.getAction())||
                        Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())){
                        ShutDownWakeLock.releaseCpuLock();
                        myHandler.removeCallbacks(myRunnable);
                        mplayer.stop();
                        if(mReceiver != null) {
                            try {
                                unregisterReceiver(mReceiver);
                            } catch (IllegalArgumentException e) {
                                Slog.e(TAG, "Exception: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                        finish();
                    }
                }
            };

         IntentFilter filter=new IntentFilter(Intent.ACTION_POWER_CONNECTED);
         filter.addAction(Intent.ACTION_BATTERY_OKAY);
         registerReceiver(mReceiver, filter);
        }

        PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String ignored) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    ShutDownWakeLock.releaseCpuLock();
                    myHandler.removeCallbacks(myRunnable);
                    if(mplayer != null){
                        mplayer.stop();
                    }
                    finish();
                }
            }
        };

        TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(new View(this));
        mDialog=new AlertDialog.Builder(this,android.R.style.Theme_Holo_Light_Dialog_MinWidth).create();
        mDialog.getWindow().setBackgroundDrawableResource(com.android.internal.R.color.transparent);
        mDialog.setTitle(com.android.internal.R.string.power_off);
        // Modify Bug 332142, Distinguish between singular and plura
        if (mSeconds != 0) {
            mDialog.setMessage(getResources().getQuantityString(
                    com.android.internal.R.plurals.shutdown_after_seconds_plurals, mSeconds,
                    mSeconds));
        } else {
            mDialog.setMessage(getString(com.android.internal.R.string.shutdown_confirm));
        }
        // bug 332142 end
        mDialog.setButton(DialogInterface.BUTTON_NEUTRAL,getText(com.android.internal.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        myHandler.removeCallbacks(myRunnable);
                        dialog.cancel();
                        mplayer.stop();
                        if(mReceiver != null) {
                            try {
                                unregisterReceiver(mReceiver);
                            } catch (IllegalArgumentException e) {
                                Slog.e(TAG, "Exception: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                        finish();
                    }});
        mDialog.setCancelable(false);
        // SPRD: setTitle ShutDownTiming.
        mDialog.getWindow().getAttributes().setTitle("ShutDownTiming");
        // SPRD: Add for BUG 271066 @{
        mDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        // @}
        mDialog.show();
        if(mConfirm == false){
            myHandler.postDelayed(myRunnable, 1000);
        }
        /* SPRD: Modify  MediaPlayer play process for bug 257397@ { */
        myHandler.post(new Runnable() {
            public void run() {

                final ContentResolver cr = getContentResolver();
                String path = Settings.System.getString(cr, Settings.System.NOTIFICATION_SOUND);
                Uri ringtoneUri = null;
                if (!TextUtils.isEmpty(path)) {
                    ringtoneUri = Uri.parse(path);
                }
                try {
                    /* SPRD: ringtone,changed for bug303604 @{ */
                    if (mAudioManager.getRingerMode() != AudioManager.RINGER_MODE_VIBRATE &&
                           mAudioManager.getRingerMode() != AudioManager.RINGER_MODE_SILENT) {
                    /* @} */
                        mplayer.reset();
                        if (null != ringtoneUri) {
                            /* SPRD fix bug395578 If an exception occurs, set the default ringtone @{ */
                            try {
                                mplayer.setDataSource(context, ringtoneUri);
                            } catch (Exception e) {
                                Slog.e(TAG, "If an exception occurs, set the default ringtone");
                                mplayer.setDataSource(context,
                                        Uri.parse("/system/media/audio/notifications/Heaven.ogg"));
                            }
                            /* @} */
                        } else {
                            mplayer.setDataSource(context,
                                    Uri.parse("/system/media/audio/notifications/Heaven.ogg"));
                        }
                        mplayer.prepare();
                        mplayer.start();
                        Slog.e(TAG, "mplayer start play ");
                    }
                } catch (IOException e) {
                    Slog.e(TAG, "e =  " + e.toString());
                }

            }
        }
        );
        /* SPRD: @} */
    }
}
