package com.sprd.keyguard;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

public class CuccSupport {
    private static CuccSupport sInstance;
    public static boolean supportCucc = false;
    private static Context sContext;
    private CuccSupport(Context context) {
        sContext = context;
    }
    public static CuccSupport getInstance(Context context) {
        if ( sInstance == null) {
            sInstance = new CuccSupport(context);
        }
        return sInstance;
    }
    public boolean makeEmergencyInvisible() {
        if (supportCucc) {
            boolean hasIccCard = false;
            TelephonyManager telePhonyManager = TelephonyManager.from(sContext);
            int numPhones = telePhonyManager.getPhoneCount();
            for (int i = 0; i < numPhones; i++) {
                hasIccCard |= telePhonyManager.hasIccCard(i);
            }
            if (!hasIccCard) {
                return true;
            }
            return false;
        }
        return false;
    }

    public CharSequence concatenateForCUCC(CharSequence plmn, CharSequence spn) {
        boolean plmnValid = !TextUtils.isEmpty(plmn);
        boolean spnValid = !TextUtils.isEmpty(spn);
        return plmnValid ? plmn : (spnValid ? spn : "");
    }

}
