package com.sprd.keyguard;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.telephony.SubscriptionInfo;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.IccCardConstants.State;
import com.android.internal.widget.LockPatternUtils;
import com.android.keyguard.KeyguardUpdateMonitorCallback;
import com.android.keyguard.KeyguardUpdateMonitor;
import com.android.keyguard.R;

public class CarriersTextLayout extends LinearLayout {
    private State[] mSimStates;
    private TextView[] mCarrierViews;
    private static final boolean DEBUG = true;
    private static final String TAG = "CarriersTextLayout";
    private KeyguardUpdateMonitor mKeyguardUpdateMonitor;
    private LockPatternUtils mLockPatternUtils;
    private static CharSequence mSeparator;
    private Context mContext;
    private String[] mCarrierName;
    private KeyguardUpdateMonitorCallback mCallback = new KeyguardUpdateMonitorCallback() {

        @Override
        public void onRefreshCarrierInfo() {
            updateCarrierText();
        }

        @Override
        public void onSimStateChanged(int subId, int slotId, IccCardConstants.State simState) {
            if (DEBUG)
                Log.d(TAG, "onSimStateChanged implements: simState = "
                        + simState + ", subId=" + subId+";slotId ="+slotId);
            /*SPRD: fix bug 432692 ,AndroidRuntime: java.lang.ArrayIndexOutOfBoundsException @{*/
            if (SubscriptionManager.isValidSlotId(slotId)) {
                mSimStates[slotId] = simState;
                updateCarrierText();
            }
            /*SPRD: fix bug 432692 ,AndroidRuntime: java.lang.ArrayIndexOutOfBoundsException @}*/
        }
    };
    /**
     * The status of this lock screen. Primarily used for widgets on LockScreen.
     */
    public CarriersTextLayout(Context context) {
        this(context, null);
    }

    public CarriersTextLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mLockPatternUtils = new LockPatternUtils(context);
        mContext = context;
    }

    protected void updateCarrierText() {
        List<SubscriptionInfo> subs = mKeyguardUpdateMonitor.getSubscriptionInfo(false);
        final int N = subs.size();
        if (DEBUG) Log.d(TAG, "updateCarrierText(): " + N);
        boolean isAllCardsAbsent = true;
        if (N == 0) {
            CharSequence displayText =  makeCarrierStringOnEmergencyCapable(
                    getContext().getText(R.string.keyguard_missing_sim_message_short),
                    getContext().getText(com.android.internal.R.string.emergency_calls_only));
            mCarrierViews[0].setText(displayText);
            mCarrierViews[0].setVisibility(View.VISIBLE);
            mCarrierViews[0].setTextColor(getResources().getColor(com.android.internal.R.color.secondary_text_dark));
        } else {
            for (int i = 0; i < N; i++) {
                CharSequence carrierName = subs.get(i).getCarrierName();
                /* SPRD: modify for bug 434366 @{ */
                int phoneId = subs.get(i).getSimSlotIndex();
                isAllCardsAbsent = isAllCardsAbsent && (mSimStates[phoneId] == IccCardConstants.State.ABSENT);
                /* @} */
                if (DEBUG) Log.d(TAG, "Handling carrierName = " + carrierName);
                if (isAllCardsAbsent) {
                    mCarrierViews[0].setText(carrierName);
                    mCarrierViews[0].setVisibility(View.VISIBLE);
                    mCarrierViews[0].setTextColor(getResources().getColor(com.android.internal.R.color.secondary_text_dark));
                } else {

                    if (mCarrierName != null && !TextUtils.isEmpty(mCarrierName[phoneId])) {
                        // SPRD: modify for bug 434366
                        Log.d(TAG, "mCarrierName" + "[" + phoneId + "] :" + mCarrierName[phoneId]);
                        mCarrierViews[i].setText(mCarrierName[phoneId]);
                        mCarrierViews[i].setVisibility(View.VISIBLE);
                    } else {
                        Log.d(TAG, "carrierName :" + carrierName);
                        mCarrierViews[i].setText(carrierName);
                        mCarrierViews[i].setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    /*SPRD :fix bug 434996,The string without translation when switch system language @{*/
    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateCarrierText();
    }
    /* SPRD :fix bug 434996,The string without translation when switch system language @}*/

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mSeparator = getResources().getString(
                com.android.internal.R.string.kg_text_message_separator);
        int phoneCount = TelephonyManager.from(mContext).getPhoneCount();
        mSimStates = new State[phoneCount];
        mCarrierViews = new TextView[phoneCount];
        for (int i = 0; i < phoneCount; i++) {
            TextView carrier = new TextView(getContext());
            carrier.setGravity(Gravity.CENTER_HORIZONTAL);
            carrier.setSingleLine();
            carrier.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            carrier.setTextAppearance(mContext, android.R.attr.textAppearanceMedium);
            addView(carrier);
            mCarrierViews[i] = carrier;
        }
        setSelected(true); // Allow marquee to work.
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mKeyguardUpdateMonitor = KeyguardUpdateMonitor.getInstance(mContext);
        mKeyguardUpdateMonitor.registerCallback(mCallback);
        TelephonyManager tgr = TelephonyManager.from(mContext);
        mCarrierName = new String[tgr.getPhoneCount()];
        registerListeners();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        KeyguardUpdateMonitor.getInstance(mContext).removeCallback(mCallback);
        mContext.unregisterReceiver(mBroadcastReceiver);
    }

    /*
     * Add emergencyCallMessage to carrier string only if phone supports emergency calls.
     */
    private CharSequence makeCarrierStringOnEmergencyCapable(
            CharSequence simMessage, CharSequence emergencyCallMessage) {
        if (mLockPatternUtils.isEmergencyCallCapable()) {
            return concatenate(simMessage, emergencyCallMessage);
        }
        return simMessage;
    }

    private static CharSequence concatenate(CharSequence plmn, CharSequence spn) {
        final boolean plmnValid = !TextUtils.isEmpty(plmn);
        final boolean spnValid = !TextUtils.isEmpty(spn);
        if (plmnValid && spnValid) {
            if (plmn.equals(spn)) {
                return plmn;
            } else {
                return new StringBuilder().append(plmn).append(mSeparator).append(spn).toString();
            }
        } else if (plmnValid) {
            return plmn;
        } else if (spnValid) {
            return spn;
        } else {
            return "";
        }
    }
    /* SPRD: Listen to call state. see bug #429277. @{ */
    private void registerListeners() {
        // broadcasts
        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyIntents.SPN_STRINGS_UPDATED_ACTION);
        mContext.registerReceiver(mBroadcastReceiver, filter);
    }
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            int phoneId ;
            if ( action.equals(TelephonyIntents.SPN_STRINGS_UPDATED_ACTION)) {
                updateNetworkName(intent.getBooleanExtra(TelephonyIntents.EXTRA_SHOW_SPN, false),
                        intent.getStringExtra(TelephonyIntents.EXTRA_SPN),
                        intent.getBooleanExtra(TelephonyIntents.EXTRA_SHOW_PLMN, false),
                        intent.getStringExtra(TelephonyIntents.EXTRA_PLMN),
                        intent.getIntExtra(PhoneConstants.PHONE_KEY,
                                SubscriptionManager.DEFAULT_PHONE_INDEX));
            }
        }
    };
    void updateNetworkName(boolean showSpn, String spn, boolean showPlmn, String plmn , int phoneid)
    {
        if (DEBUG) {
            Log.d(TAG, "updateNetworkName showSpn=" + showSpn + " spn=" + spn
                    + " showPlmn=" + showPlmn + " plmn=" + plmn +" PhoneId=" +phoneid);
        }
        StringBuilder str = new StringBuilder();
        if (showPlmn && plmn != null) {
            str.append(plmn);
        }
        if (!showPlmn && showSpn && spn != null) {
            str.append(spn);
        }
        if (str.length() != 0) {
            mCarrierName[phoneid] = str.toString();
        }
    }
    /* @} */
}
