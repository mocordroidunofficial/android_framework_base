/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.keyguard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.UserHandle;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.android.internal.telephony.IccCardConstants.State;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.widget.LockPatternUtils;
import com.sprd.keyguard.CuccSupport;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionManager.OnSubscriptionsChangedListener;
import android.util.Log;

/**
 * This class implements a smart emergency button that updates itself based
 * on telephony state.  When the phone is idle, it is an emergency call button.
 * When there's a call in progress, it presents an appropriate message and
 * allows the user to return to the call.
 */
public class EmergencyButton extends Button {
    private static final String ACTION_EMERGENCY_DIAL = "com.android.phone.EmergencyDialer.DIAL";

    /* fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature @{ */
    private static final String TAG = "EmergencyButtons";
    private static final boolean DEBUG = /* Debug.isDebug() */true;
    private int mNumPhones;
    private TelephonyManager mTelephonyManager;
    private SubscriptionManager  mSubscriptionManager;
    private PhoneStateListener[] mPhoneStateListeners;
    ServiceState[] mServiceStates;
    private int[] mSubIds;
    private SubInfoUpdateReceiver mSubInfoUpdateReceiver;
    /* @} */

    KeyguardUpdateMonitorCallback mInfoCallback = new KeyguardUpdateMonitorCallback() {

        @Override
        public void onSimStateChanged(int subId, int slotId, State simState) {
            /* fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature @{ */
            if ( mSubscriptionManager == null ) {
                mSubscriptionManager = SubscriptionManager.from(mContext);
            }
            mSubIds =mSubscriptionManager.getActiveSubscriptionIdList();
            Log.d(TAG, "onSimStateChanged(), mSubIds = " + mSubIds);
            /* @} */
            updateEmergencyCallButton();
        }

        @Override
        public void onPhoneStateChanged(int phoneState) {
            updateEmergencyCallButton();
        }
    };
    private LockPatternUtils mLockPatternUtils;
    private PowerManager mPowerManager;

    public EmergencyButton(Context context) {
        this(context, null);
    }

    public EmergencyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        KeyguardUpdateMonitor.getInstance(mContext).registerCallback(mInfoCallback);
        /* fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature
        IntentFilter intentFilter = new IntentFilter(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED);
        if (mSubInfoUpdateReceiver == null) {
            mSubInfoUpdateReceiver = new SubInfoUpdateReceiver();
        }
        mContext.registerReceiver(mSubInfoUpdateReceiver, intentFilter);
        @} */
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        KeyguardUpdateMonitor.getInstance(mContext).removeCallback(mInfoCallback);
        /* SPRD: fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature
        listenPhoneState(PhoneStateListener.LISTEN_NONE);
        for (int i = 0; i < mPhoneStateListeners.length; i++) {
            mPhoneStateListeners[i] = null;
        }
        mContext.unregisterReceiver(mSubInfoUpdateReceiver);
        mSubInfoUpdateReceiver = null;
        @} */
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLockPatternUtils = new LockPatternUtils(mContext);
        mPowerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        /* fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature @{ */
        mTelephonyManager = TelephonyManager.from(mContext);
        mSubscriptionManager = SubscriptionManager.from(mContext);
        mNumPhones =TelephonyManager.from(mContext).getPhoneCount();
        mServiceStates = new ServiceState[mNumPhones];
        mPhoneStateListeners = new PhoneStateListener[mNumPhones];
        changeEmergencyState();
        /* @} */
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                takeEmergencyCallAction();
            }
        });
        updateEmergencyCallButton();
    }

    /**
     * Shows the emergency dialer or returns the user to the existing call.
     */
    public void takeEmergencyCallAction() {
        // TODO: implement a shorter timeout once new PowerManager API is ready.
        // should be the equivalent to the old userActivity(EMERGENCY_CALL_TIMEOUT)
        mPowerManager.userActivity(SystemClock.uptimeMillis(), true);
        if (mLockPatternUtils.isInCall()) {
            mLockPatternUtils.resumeCall();
        } else {
            final boolean bypassHandler = true;
            KeyguardUpdateMonitor.getInstance(mContext).reportEmergencyCallAction(bypassHandler);
            Intent intent = new Intent(ACTION_EMERGENCY_DIAL);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            getContext().startActivityAsUser(intent,
                    new UserHandle(mLockPatternUtils.getCurrentUser()));
        }
    }

    private void updateEmergencyCallButton() {
        /* fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature @{
        boolean enabled = false;
        if (mLockPatternUtils.isInCall()) {
            enabled = true; // always show "return to call" if phone is off-hook
        } else if (mLockPatternUtils.isEmergencyCallCapable()) {
            final boolean simLocked = KeyguardUpdateMonitor.getInstance(mContext).isSimPinVoiceSecure();
            if (simLocked) {
                // Some countries can't handle emergency calls while SIM is locked.
                enabled = mLockPatternUtils.isEmergencyCallEnabledWhileSimLocked();
            } else {
                // True if we need to show a secure screen (pin/pattern/SIM pin/SIM puk);
                // hides emergency button on "Slide" screen if device is not secure.
                enabled = mLockPatternUtils.isSecure();
            }
        } */
        mLockPatternUtils.updateEmergencyCallButtonState(this, true, false);
        changeEmergencyState();
        if (CuccSupport.getInstance(mContext).makeEmergencyInvisible()) {
            setVisibility(View.INVISIBLE);
        }
        /* @} */
    }
    /* fixbug420014 Add EmergencyButton on the Lockscreen and porting the Emeegencybutton feature @{ */
    private PhoneStateListener getPhoneStateListener(int subId) {
        final int phoneId = getPhoneId(subId);

        if (mPhoneStateListeners[phoneId] == null) {
            mPhoneStateListeners[phoneId] = new PhoneStateListener(subId) {
                @Override
                public void onServiceStateChanged(ServiceState state) {
                    if (state != null) {
                        if (DEBUG)
                        Log.d(TAG, "onServiceStateChanged(), serviceState = " + state + ", phoneId = " + phoneId);
                        mServiceStates[phoneId] = state;
                    }
                    updateEmergencyCallButton();
                }
            };
        }
        return mPhoneStateListeners[phoneId];
    }
    private boolean hasService(int phoneId) {
        if (mServiceStates[phoneId] != null) {
            switch (mServiceStates[phoneId].getState()) {
                case ServiceState.STATE_OUT_OF_SERVICE:
                    return false;
                default:
                    return true;
            }
        } else {
            return false;
        }

    }
    boolean canMakeEmergencyCall(){
        for(int i=0; i<mServiceStates.length; i++) {
            if (mServiceStates[i] != null) {
                if (mServiceStates[i].isEmergencyOnly() || hasService(i)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void listenPhoneState(int listenMode) {
        if (mSubIds == null || (mSubIds != null && mSubIds.length == 0)) {
            mTelephonyManager.listen(getInvalidSubPhoneStateListener(), listenMode);
        } else {
            for (int i = 0; i< mSubIds.length; i++) {
                mTelephonyManager.listen(getPhoneStateListener(mSubIds[i]), listenMode);
            }
        }
    }

    private int getPhoneId(int subId) {
        SubscriptionInfo subInfoRecord = mSubscriptionManager.getActiveSubscriptionInfo(subId);
        if (subInfoRecord != null) {
            int phoneid = SubscriptionManager.getSlotId(subId);
            if(phoneid >= 0){
                return phoneid;
            }else{
                Log.w(TAG, "subId is :"+ subId + ",getSlotId phoneid is:" + phoneid);
                return 0;
            }
        } else {
            Log.w(TAG, "couldn't find the phoneId");
            return 0;
        }
    }

    private void changeEmergencyState() {
        /* SPRD: fix bug 429256 ,lock screen display emergency call @{
        if (!canMakeEmergencyCall()) {
            setClickable(false);
            setText(R.string.kg_emergency_call_no_service);
        } else {*/
            setClickable(true);
            setText(R.string.kg_emergency_call_label);
        /* } @}*/
    }

    class SubInfoUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "SubInfoUpdateReceiver-onReceive");
            mSubIds = mSubscriptionManager.getActiveSubscriptionIdList();
            listenPhoneState(PhoneStateListener.LISTEN_SERVICE_STATE);
        }
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        changeEmergencyState();
    }

    private PhoneStateListener getInvalidSubPhoneStateListener () {
        if (mPhoneStateListeners[0] == null) {
            mPhoneStateListeners[0] = new PhoneStateListener() {
                @Override
                public void onServiceStateChanged(ServiceState state) {
                    super.onServiceStateChanged(state);
                    if (state != null) {
                        Log.d(TAG, "InvalidSubPhoneStateListener: onServiceStateChanged(), serviceState = " + state);
                        mServiceStates[0] = state;
                    }
                    updateEmergencyCallButton();
                }
            };
        }
        return mPhoneStateListeners[0];
    }
    /* @} */
}
