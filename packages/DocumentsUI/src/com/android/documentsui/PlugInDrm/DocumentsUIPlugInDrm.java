/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.documentsui.PlugInDrm;

import android.content.Context;
import android.net.Uri;
import android.app.Activity;
import com.android.documentsui.model.DocumentInfo;
import android.app.AddonManager;
import com.android.documentsui.R;
import android.util.Log;

public class DocumentsUIPlugInDrm {

    public final static int ERROR = 0;

    static DocumentsUIPlugInDrm sInstance;

    public DocumentsUIPlugInDrm(){
    }

    public static DocumentsUIPlugInDrm getInstance(){
        if (sInstance != null)
            return sInstance;
        sInstance = (DocumentsUIPlugInDrm) AddonManager.getDefault().getAddon(R.string.feature_documentsuiplugdrm, DocumentsUIPlugInDrm.class);
        return sInstance;
    }

    public void getDocumentsActivityContext (Context context){
        return;
    }

    public boolean alertDrmError(Activity activity , Uri uri){
        return true;
    }

    public boolean isDrmEnabled() {
        return false;
    }

    public void getDrmEnabled() {
    }

    public String getDocMimeType(Context context, String path, String mimeType){
        return mimeType;
    }

    public boolean setDocMimeType(DocumentInfo doc){
        return true;
    }

    public String getDrmFilenameFromPath(String path){
        return path;
    }

    public String getDrmPath(Context context , Uri uri){
        return null;
    }

    public boolean getIsDrm(Context context , String drmPath){
        return false;
    }

    public int getIconImage(Context context , String docMimeType , String docDisplayName){
        return ERROR;
    }
}
