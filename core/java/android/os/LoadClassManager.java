
package android.os;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import android.util.Log;

import dalvik.system.PathClassLoader;

/** @hide */
public final class LoadClassManager {

    private static SprdClassLoader mLockClassLoader = null;

    private final static String TAG = "LoadClassManager";

    private static class SprdClassLoader extends PathClassLoader {
        private static final String JAR_PATH = "system/framework/telephony-common2.jar";

        public SprdClassLoader() {
            super(JAR_PATH, ClassLoader.getSystemClassLoader());
        }
    }

    public static Class getExtClass(String classPath) {
        if (mLockClassLoader == null) {
            mLockClassLoader = new SprdClassLoader();
        }
        Class cls = null;
        try {
            Log.d(TAG, " loadClass");
            cls = mLockClassLoader
                    .loadClass(classPath);
        } catch (Exception e) {
            Log.d(TAG, "getExtClass Exception " + e.toString());
        }
        return cls;
    }

    public static Object createNewInstance(String classPath, Object... args) {
        Class cls = getExtClass(classPath);
        Object obj = null;
        try {
            if (cls != null) {
                Constructor[] cons = cls.getDeclaredConstructors();
                for (Constructor con : cons) {
                    Class[] pas = con.getParameterTypes();
                    Log.d(TAG, "--------------------------");
                    if (args.length == 0) {
                        con.setAccessible(true);
                        obj = con.newInstance();
                        break;
                    }
                    if (pas.length == args.length) {
                        boolean sameType = true;
                        for (int i = 0; i < pas.length; i++) {
                            Class pa = pas[i];
                            if (!classTypeConvert(pa).isAssignableFrom(args[i].getClass())) {
                                Log.d(TAG, "nor type same obj class: " + args[i].getClass()
                                        +
                                        "   pa: " + pa);
                                sameType = false;
                                break;
                            }
                        }
                        if (sameType) {
                            con.setAccessible(true);
                            obj = con.newInstance(args);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "CreateNewInstance Exception " + e.toString());
        }
        return obj;
    }

    private static Class classTypeConvert(Class c) {
        if (c.equals(int.class)) {
            return Integer.class;
        }
        if (c.equals(long.class)) {
            return Long.class;
        }
        if (c.equals(double.class)) {
            return Double.class;
        }
        return c;
    }

    public static Object invokeMethod(Object instance, String name, Object... args) {
        Object returnobj = null;
        try {
            if (instance == null) {
                return returnobj;
            }
            Method[] methods;
            Class objectClass;
            if (instance instanceof Class) {
                objectClass = (Class) instance;
            } else {
                objectClass = instance.getClass();
            }
            methods = objectClass.getMethods();
            for (Method method : methods) {
                if (method.getName().equals(name)
                        && method.getDeclaringClass().getName().equals(objectClass.getName())) {
                    Class[] cls = method.getParameterTypes();
                    if (cls.length == args.length) {
                        if (args.length == 0) {
                            if (Modifier.isStatic(method.getModifiers())) {
                                returnobj = method.invoke(null);
                            } else if (!(instance instanceof Class)) {
                                returnobj = method.invoke(instance);
                            }
                            break;
                        }
                        boolean sameType = true;
                        for (int i = 0; i < cls.length; i++) {
                            Class pa = cls[i];
                            if (!classTypeConvert(pa).isAssignableFrom(args[i].getClass())) {
                                sameType = false;
                                break;
                            }
                        }
                        if (sameType) {
                            if (Modifier.isStatic(method.getModifiers())) {
                                returnobj = method.invoke(null, args);
                            } else if (!(instance instanceof Class)) {
                                returnobj = method.invoke(instance, args);
                            }
                            break;
                        }
                    }
                }

            }
        } catch (Exception e) {

        }
        return returnobj;
    }
}
