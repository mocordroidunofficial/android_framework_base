package com.android.internal.os.storage;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
/* SPRD: suport double sdcard
 * add for add format internal storage
 */
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.storage.IMountService;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.internal.R;

/* SPRD: suport double sdcard
 * add for add format internal storage
 */
import java.util.ArrayList;
/**
 * Takes care of unmounting and formatting external storage.
 */
public class ExternalStorageFormatter extends Service
        implements DialogInterface.OnCancelListener {
    static final String TAG = "ExternalStorageFormatter";

    public static final String FORMAT_ONLY = "com.android.internal.os.storage.FORMAT_ONLY";
    public static final String FORMAT_AND_FACTORY_RESET = "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET";

    public static final String EXTRA_ALWAYS_RESET = "always_reset";
    // SPRD: suport double sdcard add for add format internal storage
    // {@hide}
    public static final String EXTRA_FORMAT_INTERNAL = "format_internal";
    // SPRD: suport double sdcard add for add format external storage
    // {@hide}
    public static final String EXTRA_FORMAT_EXTERNAL = "format_external";
    // SPRD: suport double sdcard add for add format usb storage
    // {@hide}
    public static final String EXTRA_FORMAT_USB = "format_usb";

    // If non-null, the volume to format. Otherwise, will use the default external storage directory
    private StorageVolume mStorageVolume;

    public static final ComponentName COMPONENT_NAME
            = new ComponentName("android", ExternalStorageFormatter.class.getName());

    // Access using getMountService()
    private IMountService mMountService = null;

    private StorageManager mStorageManager = null;

    private PowerManager.WakeLock mWakeLock;

    private ProgressDialog mProgressDialog = null;

    private boolean mFactoryReset = false;
    private boolean mAlwaysReset = false;

    private String mReason = null;
    // SPRD: suport double sdcard add state
    private boolean mFinished = false;

    /** SPRD: suport double sdcard add for add format internal storage @{ */
    private String mInternalStoragePath = null;
    private String mExternalStoragePath = null;
    private String mUsbDiskPath = null;

    private int mFormatStorageIndex = 0;
    private int mFormatStorageCount = 0;
    private StorageInfo mFormatStorageInfo = null;
    private ArrayList<StorageInfo> mFormatStorageInfos
            = new ArrayList<StorageInfo>();

    private Handler mNextStorageHandler = null;

    private static final int STORAGE_TYPE_INVALID = -1;
    private static final int STORAGE_TYPE_INTERNAL = 0;
    private static final int STORAGE_TYPE_EXTERNAL = 1;
    private static final int STORAGE_TYPE_USB = 2;
    private static final int STORAGE_TYPE_OTHER = 3;

    class StorageInfo {
        String mPath;
        int mType;
        public StorageInfo() {
            mPath = null;
            mType = STORAGE_TYPE_INVALID;
        }
        public StorageInfo(String path, int type) {
            mPath = path;
            mType = type;
        }
        public String toString() {
            return "StorageInfo [ path="+mPath+", type="+mType+" ]";
        }
    };

    private static final int MESSAGE_TYPE_UNMOUNTING = 0;
    private static final int MESSAGE_TYPE_ERASING = 1;
    private static final int MESSAGE_TYPE_FORMAT_ERROR = 2;
    private static final int MESSAGE_TYPE_BAD_REMOVAL = 3;
    private static final int MESSAGE_TYPE_CHECKING = 4;
    private static final int MESSAGE_TYPE_REMOVED = 5;
    private static final int MESSAGE_TYPE_SHARED = 6;
    private static final int MESSAGE_TYPE_UNKNOWN_STATE = 7;

    private int[] mExternalMessageIDs = {
        R.string.progress_unmounting,
        R.string.progress_erasing,
        R.string.format_error,
        R.string.media_bad_removal,
        R.string.media_checking,
        R.string.media_removed,
        R.string.media_shared,
        R.string.media_unknown_state
    };

    private int[] mInternalMessageIDs = {
        R.string.progress_unmounting1,
        R.string.progress_erasing1,
        R.string.format_error1,
        R.string.media_bad_removal1,
        R.string.media_checking1,
        R.string.media_removed1,
        R.string.media_shared1,
        R.string.media_unknown_state1
    };

    private int[] mUsbMessageIDs = {
        R.string.progress_unmounting2,
        R.string.progress_erasing2,
        R.string.format_error2,
        R.string.media_bad_removal2,
        R.string.media_checking2,
        R.string.media_removed2,
        R.string.media_shared2,
        R.string.media_unknown_state2
    };

    private int[] mOtherMessageIDs = {
        R.string.progress_unmounting_,
        R.string.progress_erasing_,
        R.string.format_error_,
        R.string.media_bad_removal_,
        R.string.media_checking_,
        R.string.media_removed_,
        R.string.media_shared_,
        R.string.media_unknown_state_
    };
    /** @} */

    StorageEventListener mStorageListener = new StorageEventListener() {
        @Override
        public void onStorageStateChanged(String path, String oldState, String newState) {
            Log.i(TAG, "Received storage state changed notification that " +
                    path + " changed state from " + oldState +
                    " to " + newState);
            /* SPRD: suport double sdcard
             * add state for do not update when finished. @{
             */
            if(mFinished){
                Log.d(TAG, "not to updateProgressState for finished :");
                return;
            }
            /* @} */
            updateProgressState();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        if (mStorageManager == null) {
            mStorageManager = (StorageManager) getSystemService(Context.STORAGE_SERVICE);
            mStorageManager.registerListener(mStorageListener);
        }

        mWakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ExternalStorageFormatter");
        mWakeLock.acquire();
    }

    /** SPRD: suport double sdcard
      * add for add format internal storage @{
      */
    private String getInternalStoragePath() {
        if (mInternalStoragePath == null) {
            if (Environment.internalIsEmulated()) {
                mInternalStoragePath = "";
            } else {
                mInternalStoragePath = Environment.getInternalStoragePath().getPath();
            }
        }

        return mInternalStoragePath;
    }

    private String getExternalStoragePath() {
        if (mExternalStoragePath == null) {
            mExternalStoragePath = Environment.getExternalStoragePath().getPath();
        }

        return mExternalStoragePath;
    }

    private String getUsbDiskPath() {
        if (mUsbDiskPath == null) {
            StorageVolume[] volumeList = mStorageManager.getVolumeList();
            for (StorageVolume volume:volumeList) {
                if (volume.isRemovable()
                    && (!volume.getPath().equals(getExternalStoragePath()))
                    && volume.getPath().contains("usb"))
                    mUsbDiskPath = volume.getPath();
            }
            if (mUsbDiskPath == null)
                mUsbDiskPath = "";
        }

        return mUsbDiskPath;
    }
    /** @} */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /** SPRD: suport double sdcard
          * add for add format internal storage @{
          */
        boolean formatInternal = false;
        boolean formatExternal = false;
        boolean formatUsb = false;
        /** @} */
        if (FORMAT_AND_FACTORY_RESET.equals(intent.getAction())) {
            mFactoryReset = true;
        }
        if (intent.getBooleanExtra(EXTRA_ALWAYS_RESET, false)) {
            mAlwaysReset = true;
        }

        mReason = intent.getStringExtra(Intent.EXTRA_REASON);

        // SPRD: suport double sdcard add state
        mFinished = false;

        mStorageVolume = intent.getParcelableExtra(StorageVolume.EXTRA_STORAGE_VOLUME);

        /** SPRD: suport double sdcard
         * add for add format internal storage @{
         */
        formatInternal = intent.getBooleanExtra(EXTRA_FORMAT_INTERNAL, false);
        formatExternal = intent.getBooleanExtra(EXTRA_FORMAT_EXTERNAL, false);
        formatUsb = intent.getBooleanExtra(EXTRA_FORMAT_USB, false);

        if (formatInternal || formatExternal || formatUsb) {
            Log.d(TAG,"onStartCommand format : "+ (formatInternal ? "internal" : "")
                + ((formatInternal & formatExternal) ? "," : "")
                + (formatExternal ? "external" : "")
                + ((formatInternal | formatExternal & formatUsb) ? "," : "")
                + (formatUsb ? "usb" : ""));
        }

        if (mFormatStorageCount > 0) {
            mFormatStorageInfos.clear();
        }

        String storagePath = null;
        if (mStorageVolume != null) {
            storagePath = mStorageVolume.getPath();
            String internalStoragePath = getInternalStoragePath();
            if (internalStoragePath != null && storagePath.equals(internalStoragePath)) {
                mFormatStorageInfos.add(new StorageInfo(storagePath, STORAGE_TYPE_INTERNAL));
            } else if (storagePath.equals(getExternalStoragePath())) {
                mFormatStorageInfos.add(new StorageInfo(storagePath, STORAGE_TYPE_EXTERNAL));
            } else if (storagePath.equals(getUsbDiskPath())) {
                mFormatStorageInfos.add(new StorageInfo(storagePath, STORAGE_TYPE_USB));
            } else {
                mFormatStorageInfos.add(new StorageInfo(storagePath, STORAGE_TYPE_OTHER));
            }
        }
        if (formatInternal == false && formatExternal == false && formatUsb == false) {
            if (mStorageVolume == null) {
                mFormatStorageInfos.add(new StorageInfo(getExternalStoragePath(), STORAGE_TYPE_EXTERNAL));
            }
        } else {
            if (formatInternal) {
                String internalStoragePath = getInternalStoragePath();
                if (!internalStoragePath.equals("")) {
                    if (storagePath == null || !internalStoragePath.equals(storagePath)) {
                        mFormatStorageInfos.add(new StorageInfo(internalStoragePath, STORAGE_TYPE_INTERNAL));
                    }
                }
            }
            if (formatExternal) {
                String externalStoragePath = getExternalStoragePath();
                if (!externalStoragePath.equals("")) {
                    if (storagePath == null || !externalStoragePath.equals(storagePath)) {
                        mFormatStorageInfos.add(new StorageInfo(externalStoragePath, STORAGE_TYPE_EXTERNAL));
                    }
                }
            }
            if (formatUsb) {
                String usbDiskPath = getUsbDiskPath();
                if (!usbDiskPath.equals("")) {
                    if (storagePath == null || !usbDiskPath.equals(storagePath)) {
                        mFormatStorageInfos.add(new StorageInfo(usbDiskPath, STORAGE_TYPE_USB));
                    }
                }
            }
        }

        mFormatStorageCount = mFormatStorageInfos.size();
        mFormatStorageIndex = 0;
        if (mFormatStorageCount > 0) {
            mFormatStorageInfo = mFormatStorageInfos.get(0);
            mNextStorageHandler = new Handler();
        }

        Log.d(TAG,"onStartCommand mFormatStorageCount is " + mFormatStorageCount);
        for (int index = 0; index < mFormatStorageCount; index++)
            Log.d(TAG,"onStartCommand mFormatStorageInfos["+index+"] is " + mFormatStorageInfos.get(index));
        /** @} */

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            if (!mAlwaysReset) {
                mProgressDialog.setOnCancelListener(this);
            }
            updateProgressState();
            mProgressDialog.show();
        }

        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        if (mStorageManager != null) {
            mStorageManager.unregisterListener(mStorageListener);
        }
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        mWakeLock.release();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        IMountService mountService = getMountService();
        /** SPRD: suport double sdcard
         * modified for add format internal storage @{
          @orig
        String extStoragePath = mStorageVolume == null ?
                Environment.getLegacyExternalStorageDirectory().toString() :
                mStorageVolume.getPath();
        try {
            mountService.mountVolume(extStoragePath);
        */
        try {
            mFormatStorageIndex = mFormatStorageCount;
            if (mFormatStorageInfo != null)
                mountService.mountVolume(mFormatStorageInfo.mPath);
        /** @} */
        } catch (RemoteException e) {
            Log.w(TAG, "Failed talking with mount service", e);
        }
        stopSelf();
    }

    void fail(int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        /** SPRD: suport double sdcard
         * modified for add format internal storage @{
          @orig
        if (mAlwaysReset) {
            Intent intent = new Intent(Intent.ACTION_MASTER_CLEAR);
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
            intent.putExtra(Intent.EXTRA_REASON, mReason);
        */
        nextStorageOrFactoryReset(mAlwaysReset);
        /** @} */
        stopSelf();
    }

    /*
     * SPRD: suport double sdcard
     * add for add format internal storage @{
     *
     * return if has next storage need to format
     */
    private boolean nextStorageOrFactoryReset(boolean factoryReset) {
        mFormatStorageIndex++;
        if (mFormatStorageIndex < mFormatStorageCount) {
            mNextStorageHandler.post(new Runnable(){
                public void run() {
                    updateProgressState();
                }
            });
            return true;
        }
        if (factoryReset) {
            // SPRD: add FLAG_RECEIVER_FOREGROUND attribute
            //sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
            sendBroadcastForeground();
        }
        mFinished = true;
        return false;
    }

    private int getMessageID(int messageType) {
        int storageType = STORAGE_TYPE_EXTERNAL;
        if (mFormatStorageInfo != null) {
            storageType = mFormatStorageInfo.mType;
        }
        if (storageType == STORAGE_TYPE_INTERNAL) {
            return mInternalMessageIDs[messageType];
        } else if (storageType == STORAGE_TYPE_EXTERNAL) {
            return mExternalMessageIDs[messageType];
        } else if (storageType == STORAGE_TYPE_USB) {
            return mUsbMessageIDs[messageType];
        } else if (storageType == STORAGE_TYPE_OTHER) {
            return mOtherMessageIDs[messageType];
        }
        return R.string.untitled;
    }
    /** @} */

    void updateProgressState() {
        /** SPRD: suport double sdcard
         * modified for add format internal storage @{
         @orig
        String status = mStorageVolume == null ?
                Environment.getExternalStorageState() :
                mStorageManager.getVolumeState(mStorageVolume.getPath());
         */
        if (mFormatStorageIndex < mFormatStorageCount) {
            mFormatStorageInfo = mFormatStorageInfos.get(mFormatStorageIndex);
        } else {
            if (mFormatStorageCount == 0 && (mFactoryReset || mAlwaysReset)) {
                // SPRD: add FLAG_RECEIVER_FOREGROUND attribute
                //sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                sendBroadcastForeground();
            }
            stopSelf();
            return;
        }
        final String formatStoragePath = mFormatStorageInfo.mPath;
        String status = mStorageManager.getVolumeState(formatStoragePath);
        /** @} */
        if (Environment.MEDIA_MOUNTED.equals(status)
                || Environment.MEDIA_MOUNTED_READ_ONLY.equals(status)) {
            /** SPRD: suport double sdcard
             * modified for add format internal storage @{
             @orig
            updateProgressDialog(R.string.progress_unmounting);
            */
            updateProgressDialog(getMessageID(MESSAGE_TYPE_UNMOUNTING));
            /** @} */
            IMountService mountService = getMountService();
            /** SPRD: suport double sdcard
             * delete for add format internal storage
             @orig
            final String extStoragePath = mStorageVolume == null ?
                    Environment.getLegacyExternalStorageDirectory().toString() :
                    mStorageVolume.getPath();
            */
            try {
                // Remove encryption mapping if this is an unmount for a factory reset.
                /** SPRD: suport double sdcard modified for add format internal storage @{
                 @orig
                mountService.unmountVolume(extStoragePath, true, mFactoryReset);
                */
                mountService.unmountVolume(formatStoragePath, true, mFactoryReset);
                /** @} */
            } catch (RemoteException e) {
                Log.w(TAG, "Failed talking with mount service", e);
            }
        } else if (Environment.MEDIA_NOFS.equals(status)
                || Environment.MEDIA_UNMOUNTED.equals(status)
                || Environment.MEDIA_UNMOUNTABLE.equals(status)) {
            /** SPRD: suport double sdcard
            * modified for add format internal storage @{
             @orig
            updateProgressDialog(R.string.progress_erasing);
            */
            updateProgressDialog(getMessageID(MESSAGE_TYPE_ERASING));
            /** @} */
            final IMountService mountService = getMountService();
            /** SPRD: suport double sdcard delete for add format internal storage
             @orig
            final String extStoragePath = mStorageVolume == null ?
                    Environment.getLegacyExternalStorageDirectory().toString() :
                    mStorageVolume.getPath();
            */
            if (mountService != null) {
                new Thread() {
                    @Override
                    public void run() {
                        boolean success = false;
                        try {
                            /** SPRD: suport double sdcard modified for add format internal storage @{
                             @orig
                            mountService.formatVolume(extStoragePath);
                             */
                            mountService.formatVolume(formatStoragePath);
                            /** @} */
                            success = true;
                        } catch (Exception e) {
                            /** SPRD: suport double sdcard modified for add format internal storage @{
                             @orig
                            Toast.makeText(ExternalStorageFormatter.this,
                                    R.string.format_error, Toast.LENGTH_LONG).show();
                             */
                            Toast.makeText(ExternalStorageFormatter.this,
                                    getMessageID(MESSAGE_TYPE_FORMAT_ERROR), Toast.LENGTH_LONG).show();
                            /** @} */
                        }
                        if (success) {
                            if (mFactoryReset) {
                                /** SPRD: suport double sdcard
                                 * modified for add format internal storage @{
                                 @orig

                                Intent intent = new Intent(Intent.ACTION_MASTER_CLEAR);
                                intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                                intent.putExtra(Intent.EXTRA_REASON, mReason);
                                sendBroadcast(intent);
                                 */
                                if (nextStorageOrFactoryReset(true)) {
                                    return;
                                }
                                /** @} */
                                // Intent handling is asynchronous -- assume it will happen soon.
                                stopSelf();
                                return;
                            }
                        }
                        // If we didn't succeed, or aren't doing a full factory
                        // reset, then it is time to remount the storage.
                        if (!success && mAlwaysReset) {
                            /** SPRD: suppport double sdcard
                             *  modified for add format internal storage @{
                             *  @orig
                            Intent intent = new Intent(Intent.ACTION_MASTER_CLEAR);
                            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                            intent.putExtra(Intent.EXTRA_REASON, mReason);
                            sendBroadcast(intent);
                             */
                            nextStorageOrFactoryReset(true);
                            /** @} */

                        } else {
                            try {
                                /** SPRD: suport double sdcard
                                 *  modified for add format internal storage @{
                                 @orig
                                mountService.mountVolume(extStoragePath);
                                 */
                                mountService.mountVolume(formatStoragePath);
                                /** @} */
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed talking with mount service", e);
                            }
                        }
                        stopSelf();
                        return;
                    }
                }.start();
            } else {
                Log.w(TAG, "Unable to locate IMountService");
            }
        } else if (Environment.MEDIA_BAD_REMOVAL.equals(status)) {
            /* SPRD: suport double sdcard
            * modified for add format internal storage @{
             @orig
            fail(R.string.media_bad_removal);
            */
            fail(getMessageID(MESSAGE_TYPE_BAD_REMOVAL));
            /** @} */
        } else if (Environment.MEDIA_CHECKING.equals(status)) {
           /* SPRD: suport double sdcard
            * modified for add format internal storage @{
             @orig
            fail(R.string.media_checking);
            */
            fail(getMessageID(MESSAGE_TYPE_CHECKING));
            /** @} */
        } else if (Environment.MEDIA_REMOVED.equals(status)) {
            /** SPRD: suport double sdcard
            * modified for add format internal storage @{
             @orig
            fail(R.string.media_removed);
            */
            fail(getMessageID(MESSAGE_TYPE_REMOVED));
            /** @} */
        } else if (Environment.MEDIA_SHARED.equals(status)) {
           /** SPRD: suport double sdcard
            * modified for add format internal storage @{
             @orig
            fail(R.string.media_shared);
            */
            fail(getMessageID(MESSAGE_TYPE_SHARED));
            /** @} */
        } else {
           /** SPRD: suport double sdcard
            * modified for add format internal storage @{
             @orig
            fail(R.string.media_unknown_state);
            */
            fail(getMessageID(MESSAGE_TYPE_UNKNOWN_STATE));
            /** @} */
            Log.w(TAG, "Unknown storage state: " + status);
            /** SPRD: suport double sdcard delete for add format internal storage
             @orig
            stopSelf();
            */
        }
    }

    public void updateProgressDialog(int msg) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            mProgressDialog.show();
        }

        mProgressDialog.setMessage(getText(msg));
    }

    IMountService getMountService() {
        if (mMountService == null) {
            IBinder service = ServiceManager.getService("mount");
            if (service != null) {
                mMountService = IMountService.Stub.asInterface(service);
            } else {
                Log.e(TAG, "Can't get mount service");
            }
        }
        return mMountService;
    }

    /* SPRD: support double sdcard
     * add FLAG_RECEIVER_FOREGROUND and EXTRA_REASON attribute @{ */
    public void sendBroadcastForeground() {
        Intent intent = new Intent("android.intent.action.MASTER_CLEAR").addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        intent.putExtra(Intent.EXTRA_REASON, mReason);
        sendBroadcast(intent);
    }
    /* @} */
}
