package com.sprd.telephony;

import android.app.AddonManager;
import android.util.Log;
import com.android.internal.R;

/**
 * for signal strength plugin bug#424124
 * @author sprd
 */
public class SignalStrengthUtilsSprd {

    private static final String TAG = "SignalStrengthUtilsSprd";
    private static SignalStrengthUtilsSprd mInstance;

    public SignalStrengthUtilsSprd(){
    }

    public synchronized static SignalStrengthUtilsSprd getInstance(){
        if(mInstance != null){
            return mInstance;
        }
        mInstance = (SignalStrengthUtilsSprd) AddonManager.getDefault().getAddon(R.string.feature_signalstrength, SignalStrengthUtilsSprd.class);
        return mInstance;
    }

    public int processLteLevel(int level, int lteRsrp){
        Log.d(TAG, "processLteLevel: empty method");
        return level;
    }
}